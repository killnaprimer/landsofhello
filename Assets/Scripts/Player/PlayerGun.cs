﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGun : MonoBehaviour {
	public LayerMask layerMask;
	public GameObject[] projectiles;

	public int curr_projectile; 

	public float shotTime = 1f;
	float nextShotTime = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Mouse0)) {
			Shoot ();
		}
	}

	void Shoot () {
		if (nextShotTime < Time.time) {
			GetComponent<SpeechController> ().SayBang ();
			GameObject proj = GameObject.Instantiate (projectiles [curr_projectile]);
			proj.transform.position = transform.position + new Vector3(0,0.5f,0);
			proj.transform.rotation = transform.rotation;
				
			proj.GetComponent<Projectile> ().Launch ();
			nextShotTime = Time.time + shotTime;
		}

	}
}
