﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerResource : MonoBehaviour {
	public int[] resources;
	// Use this for initialization
	void Start () {
		resources [0] = 0;
		resources [1] = 0;
		resources [2] = 1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ChangeResource(ResourceUnit res) {
		GetComponent<SpeechController> ().SayPickup ();
		resources [res.id] = resources [res.id]  + res.amount;
	}

	public int GetResourceAvaliable(int resId) {
		//if (resources [resId] != null) {
			return resources [resId];
		//}
		//return 0;
	}
}
