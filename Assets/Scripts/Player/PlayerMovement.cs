﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	public float speed = 18.0f;
	public float run_speed = 20f;
	float active_speed;
	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		active_speed = speed;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.LeftShift)) {
			active_speed = run_speed;
		} else {
			active_speed = speed;
		}

		float translation = Input.GetAxis ("Vertical") * active_speed;
		float strafe = Input.GetAxis ("Horizontal") * active_speed;
		translation *= Time.deltaTime;
		strafe *= Time.deltaTime;

		transform.Translate (strafe, 0, translation);
	}
}
