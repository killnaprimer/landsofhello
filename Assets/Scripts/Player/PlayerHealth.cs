﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {
	public int max_health = 5;
	public int health = 5;
	public Vector3 currentSavePoint;
	public int lifes = 3;
	// Use this for initialization
	void Start () {
		health = max_health;
		currentSavePoint = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GetDamage(int dmg) {
		health = health - dmg;
		if (health <= 0) {
			Die();
		}
		if (health > max_health) {
			health = max_health;
		}
	}


	public void Die() {
		lifes = lifes - 1;
		if (lifes <= 0) SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		RefillHealth ();
		transform.position = currentSavePoint;
	}

	public void Sacrifice() {
		lifes = lifes - 1;
		if (lifes <= 0) SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		RefillHealth ();
		//transform.position = currentSavePoint;
	}

	public void SetSavePoint(Vector3 newSavePoint) {
		currentSavePoint = newSavePoint;
		RefillHealth ();
	}

	void RefillHealth() {
		health = max_health;
	}
}
