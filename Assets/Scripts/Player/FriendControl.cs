﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendControl : MonoBehaviour {
	public List<Unit> Units;
	public Transform flag;
	public LayerMask layerMask;
	public GameObject sphere;
	public Vector3 sphereSize;
	public float callRadius = 15f;
	// Use this for initialization
	void Start () {
		sphereSize = sphere.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		TriggerSphere ();
		if (Input.GetKeyDown(KeyCode.E)) {
			CallForFriends ();

		}

		if (Input.GetKeyDown(KeyCode.Q)) {
			MoveFlag ();
			SendFriends ();
		}

		if (Input.GetKeyDown(KeyCode.Mouse1)){
			CallOneFriend ();
		}
	}

	void CallForFriends () {
		GetComponent<SpeechController> ().SayHello ();

		sphereSize = new Vector3 (callRadius+2, callRadius/10, callRadius+2);
		Collider[] hitColliders = Physics.OverlapSphere (transform.position, callRadius);
		foreach (Collider col in hitColliders) {
			if (col.gameObject.tag == "Friend") {
				col.GetComponent<Unit> ().SetMoveTarget (transform, true);
			}

		}
	}


	void CallOneFriend () {


		RaycastHit hit;
		if (Physics.Raycast (Camera.main.transform.position, Camera.main.transform.forward, out hit, Mathf.Infinity)) {
			if (hit.collider.gameObject.tag == "Friend") {
				GetComponent<SpeechController> ().SayHello ();
				hit.collider.gameObject.GetComponent<Unit> ().SetMoveTarget (transform, true);
			}
			if (hit.collider.gameObject.tag == "FriendSpawn") {
				//GetComponent<SpeechController> ().SayPickup ();
				hit.collider.gameObject.SendMessage ("SpawnDrop", SendMessageOptions.DontRequireReceiver);
			}
		}

	}

	void SendFriends () {
		GetComponent<SpeechController> ().SayOk ();

		Collider[] hitColliders = Physics.OverlapSphere (transform.position, 15f);
		foreach (Collider col in hitColliders) {
			if (col.gameObject.tag == "Friend") {
				col.GetComponent<Unit> ().SetMoveTarget (flag, false);
			}

		}
	}

	void MoveFlag () {
		RaycastHit hit;
		if (Physics.Raycast (Camera.main.transform.position, Camera.main.transform.forward, out hit, Mathf.Infinity, layerMask)) {
			flag.position = hit.point;
		}

	}

	void TriggerSphere () {
		
		sphere.transform.localScale = Vector3.Lerp (sphere.transform.localScale, sphereSize, 0.3f);
		if (sphere.transform.localScale == sphereSize) {
			sphereSize = new Vector3 (1, 1, 1);
		}

	}
}