﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_logic : MonoBehaviour {
	public Text res_text;
	public Text hp_text;
	public Text lifes_text;
	public GameObject help_panel;
	PlayerResource resource;
	PlayerHealth health;
	// Use this for initialization
	void Start () {
		resource = GetComponentInParent<PlayerResource> ();
		health = GetComponentInParent<PlayerHealth> ();
	}
	
	// Update is called once per frame
	void Update () {
		res_text.text = "Gold: " + resource.GetResourceAvaliable(0);
		hp_text.text = "HP: " + health.health;
		lifes_text.text = "Lifes: " + health.lifes;

		if (Input.GetKeyDown (KeyCode.F1)) {
			help_panel.SetActive (!help_panel.activeSelf);
		}
	}
}
