﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
	public GameObject projectile;
	public GameObject target;
	public float shotTime = 3f;
	public float nextShotTime = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Shoot ();
	}

	void Shoot () {
		if ((target != null)&&(nextShotTime < Time.time)) {
			gameObject.transform.LookAt (target.transform.position);
			GameObject proj = GameObject.Instantiate (projectile);
			proj.transform.position = transform.position;
			proj.transform.LookAt(target.transform.position);
			proj.GetComponent<Projectile> ().Launch ();
			nextShotTime = Time.time + shotTime;
		}
	}

	public void SetTarget(GameObject newTarget) {
		target = newTarget;
	}

}