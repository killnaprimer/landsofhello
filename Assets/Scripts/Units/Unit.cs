﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Unit : MonoBehaviour {
	NavMeshAgent agent;
	GameObject atkTarget;
	Transform moveTarget;
	Animator animator;
	SpeechController speechController;

	public float radius = 10f;
	public float atkRadius = 5f;
	public List<string> targetTags;

	public Weapon weapon;
	public bool followTarget = false;


	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent> ();
		weapon = GetComponentInChildren<Weapon> ();
		animator = GetComponent<Animator> ();
		speechController = GetComponent<SpeechController> ();
	}
	
	// Update is called once per frame
	void Update () {
		LookForTarget ();
		GoToTarget ();
		AtkTarget ();
	}

	void GoToTarget () {
		
		if (moveTarget != null) {
			agent.stoppingDistance = 2f;
			agent.SetDestination (moveTarget.position);
			if ((agent.remainingDistance <= 2f) && (!followTarget))  {
				moveTarget = null;
			}
		
		} else {
			agent.stoppingDistance = atkRadius;
			if (atkTarget != null) {
				agent.SetDestination (atkTarget.transform.position);
			}
		}
	}
		
	void AtkTarget () {
		if (atkTarget != null) {
			if (Vector3.Distance (transform.position, atkTarget.transform.position) <= atkRadius) {
				weapon.SetTarget (atkTarget);
				//GameObject.Destroy (atkTarget);
			} else {
				weapon.SetTarget (null);
			}
		}
	}

	void LookForTarget () {
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
		foreach (Collider col in hitColliders) {
			List<GameObject> avaliableTargets = new List<GameObject> ();

			if (CompareTags (col.gameObject)) {
				avaliableTargets.Add (col.gameObject);
			}

			if (avaliableTargets.Count > 0) {
				atkTarget = avaliableTargets [0];
			}
		}
	}

	public void SetMoveTarget (Transform newMoveTarget, bool follow) {
		ActivateVisuals ();
		followTarget = follow;
		moveTarget = newMoveTarget;
		if (follow) {
			speechController.SayHello ();
		} else {
			speechController.SayOk ();
		}

	}

	void ActivateVisuals() {
		if (animator != null) animator.SetTrigger ("onHello");

	}

	bool CompareTags (GameObject target) {
		foreach (string tag in targetTags) {
			if (tag == target.tag) {
				return true;
			}
		}
		return false;
	}

	void OnDrawGizmosSelected(){
		Gizmos.color = Color.white;
		Gizmos.DrawWireSphere(transform.position, radius);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, atkRadius);
	}
}
