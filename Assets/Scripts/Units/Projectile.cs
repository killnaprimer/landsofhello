﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
	public int dmg = 1;
	public float speed = 200f;
	public float destroyDelay = 0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
	public void Launch () {
		GetComponent<TrailRenderer> ().Clear ();
		GetComponent<TrailRenderer> ().enabled = true;
		GetComponent<Rigidbody> ().AddForce (transform.forward * speed);
	}

	void OnCollisionEnter (Collision col) {
		col.gameObject.SendMessage ("GetDamage", dmg, SendMessageOptions.DontRequireReceiver);
		GameObject.Destroy (gameObject, destroyDelay);
	}
}
