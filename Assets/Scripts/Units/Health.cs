﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
	public int health = 10;
	public GameObject drop;
	SpeechController speechController;

	// Use this for initialization
	void Start () {
		speechController = GetComponent<SpeechController> ();
	}
	
	// Update is called once per frame
	void Update () {
		//print (transform.position);
	}

	public void GetDamage(int dmg) {
		health = health - dmg;
		if ((dmg > 0) && (speechController != null)) {
			speechController.SayOuch ();
		}
		if (health <= 0) {
			//gameObject.SetActive (false);
			SpawnDrop ();
			GameObject.Destroy (gameObject);
		}
	}

	void SpawnDrop() {
		if (drop != null) {
			GameObject newDrop = GameObject.Instantiate (drop);
			newDrop.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
		}
	}
}
