﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SacrificeSpawn : MonoBehaviour {
	public GameObject spawnObject;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			PlayerHealth ph = other.gameObject.GetComponent<PlayerHealth> ();
			ph.Sacrifice ();
			spawnObject.SetActive (true);
			gameObject.SetActive (false);
		}
	}
}
