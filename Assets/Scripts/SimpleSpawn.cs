﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleSpawn : MonoBehaviour {
	public GameObject drop;
	public Transform SpawnPoint;
	public int res_id = 0;
	public int res_price = 2;
	public int price_growth = 1;
	public string res_name = "gold";
	public float max_offset = 1f;

	public Text text;
	public int curr_price;


	// Use this for initialization
	void Start () {
		text = GetComponentInChildren<Text> ();
		curr_price = res_price;
		UpdateText ();

	}


	void UpdateText() {
		text.text = "Summon " + drop.name + ":\n-----------\n" + curr_price + res_name;
	}

	// Update is called once per frame
	void Update () {
		
	}
	/*
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			GameObject newDrop = GameObject.Instantiate (drop);
			drop.transform.position = transform.position;
			GameObject.Destroy (gameObject);
		}

	}
	*/

	public void SpawnDrop() {
		PlayerResource playerRes = FindObjectOfType<PlayerResource> ();

		if (playerRes.GetResourceAvaliable (res_id) >= curr_price) {
			ResourceUnit resource = new ResourceUnit ();
			resource.id = res_id;
			resource.amount = -curr_price;
			playerRes.ChangeResource (resource);
			GameObject newDrop = GameObject.Instantiate (drop);
			//Random.InitState (123);
			Vector3 offset = new Vector3 (Random.Range (-max_offset, max_offset), 0, Random.Range (-max_offset, max_offset)); 
			drop.transform.position = SpawnPoint.position + offset;
			curr_price += price_growth;

		}
		UpdateText ();
	}

	public void DropPrice() {
		curr_price = res_price;
		UpdateText ();
	}
		
}
