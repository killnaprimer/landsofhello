﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechController : MonoBehaviour {
	AudioSource audio;

	//----------------------------
	public float pitch_min = 1f;
	public float pitch_max = 1f;
	public AudioClip hello_clip;
	public AudioClip ok_clip;
	public AudioClip hit_clip;
	public AudioClip shot_clip;
	public AudioClip pickup_clip;
	public float reactionDelay = 0.2f;
	public float reactionSpread = 0f;


	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		if (audio != null) {
			audio.pitch = (Random.Range (pitch_min, pitch_max));
		}
	}

	public void SayHello() {
		if (audio != null) {
			audio.volume = 1f;
			audio.pitch = (Random.Range (pitch_min, pitch_max));
			audio.clip = hello_clip;
			audio.PlayDelayed (reactionDelay + Random.Range (0f, reactionSpread));
		}
	}

	public void SayOk() {
		if (audio != null) {
			audio.volume = 1f;
			audio.pitch = (Random.Range (pitch_min, pitch_max));
			audio.clip = ok_clip;
			audio.PlayDelayed (reactionDelay + Random.Range (0f, reactionSpread));
		}
	}

	public void SayOuch() {
		if (audio != null) {
			audio.volume = .5f;
			audio.pitch = (Random.Range (pitch_min, pitch_max));
			audio.clip = hit_clip;
			audio.PlayDelayed (Random.Range (0f, reactionDelay));
		}
	}

	public void SayBang() {
		if (audio != null) {
			audio.volume = .25f;
			audio.pitch = 1f;
			audio.clip = shot_clip;
			audio.PlayDelayed (reactionDelay + Random.Range (0f, reactionSpread));
		}
	}

	public void SayPickup() {
		if (audio != null) {
			audio.volume = .25f;
			audio.pitch = (Random.Range (pitch_min, pitch_max));//audio.pitch = 1f;
			audio.clip = pickup_clip;
			audio.PlayDelayed (reactionDelay + Random.Range (0f, reactionSpread));
		}
	}


}
