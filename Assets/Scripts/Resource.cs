﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceUnit {
	public int id;
	public int amount;
}



public class Resource : MonoBehaviour {
	public int id = 1;
	public int amount = 1;

	// Update is called once per frame
	void Update () {
		transform.Rotate (new Vector3 (0, 5, 0));
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {

			ResourceUnit res = new ResourceUnit ();
			res.id = id;
			res.amount = amount;

			other.gameObject.SendMessage ("ChangeResource", res, SendMessageOptions.DontRequireReceiver);
			GameObject.Destroy (gameObject);
		}
	}
}
